const express = require('express');
const router = express.Router();
const fs = require('fs');
const utilities = require('./utilities');
const request = utilities.promisify(require('request'));
const mkdirp = utilities.promisify(require('mkdirp'));
const readFile = utilities.promisify(fs.readFile);
const writeFile = utilities.promisify(fs.writeFile);
const path = require('path');

function download(url, filename) {
    console.log(`Downloading ${url}`);
    let body;

    return request(url)
        .then(response => {
            body = response.body;
            return mkdirp(path.dirname(filename));
        })
        .then(() => writeFile(filename, body))
        .then(() => {
            console.log(`Download and saved: ${url}`);
            return body;
        })
}

function spiderLinks(currentUrl, body, nesting) {
    let promise = Promise.resolve();
    if (nesting === 0) {
        return promise;
    }
    const links = utilities.getPageLinks(currentUrl, body);
    links.forEach(link => {
        promise = promise.then(() => spider(link, nesting - 1));
    });
    return promise;
}

function spider(url, nesting) {
    const filename = utilities.urlToFilename(url);
    return readFile(filename, 'utf-8')
        .then((body) => (spiderLinks(url, body, nesting)),
            (err) => {
                if(err.code !== 'ENOENT') {
                    throw err;
                }

                return download(url, filename)
                    .then(body => spiderLinks(url, body, nesting))
                    ;
            });
}

spider('https://elegro.io/', 1)
    .then(() => console.log('Download complete'))
    .catch(err => console.log(err));

module.exports = router;
